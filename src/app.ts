import app, { PORT } from "./app/index";

app.listen(PORT, () => {
  console.log(`app is running on port ${PORT}`);
});

import express, { Request, Response } from "express";

import * as dotenv from "dotenv";

import { route } from "./routes";

dotenv.config();

const app = express();
const port = process.env.PORT;

route(app);

export const PORT = port;
export default app;

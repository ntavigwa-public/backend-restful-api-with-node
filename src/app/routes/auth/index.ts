import { Request, Response, Router } from "express";

/**
 * @class
 * @author Ntavigwa Bashombe
 * @since 0.001
 */
class Auth {
  public router: Router = Router();
  /**
   * @constructor
   * @author Ntavigwa Bashombe
   * @since 0.001
   */
  constructor() {
    this.initRoute();
  }
  /**
   * @author Ntavigwa Bashombe
   * @since 0.001
   */
  initRoute() {
    this.router.post("/auth/login", (req: Request, res: Response) => {
      res.send("Login Page");
    });
    this.router.post("/auth/signup", (req: Request, res: Response) => {
      res.send("Register Page");
    });
  }
}

export const auth = new Auth().router;

import { Request, Response, Application } from "express";

import { auth } from "./auth";

import Sys from "../system";

const URL_PREFIX = "/api/v1";

const { Notification, Responses } = Sys;

/**
 * @class Routes
 */
class Routes {
  /**
   * Router
   * @author Ntavigwa Bashombe
   * @since 0.001
   *
   * @param {Application} app - express application
   * @return {void} returns nothing
   * @memberof Routes
   */
  public router = (app: Application) => {
    app.use(URL_PREFIX, auth);
    app.get("/", (req: Request, res: Response) => {
      Responses.ok(
        Notification.httpCode.ok(),
        Notification.httpMessage.welcomHomePageMessage(),
        res
      );
    });
    app.all("*", (req: Request, res: Response) => {
      Responses.error(
        Notification.httpCode.notFound(),
        Notification.httpMessage.PageNotFoundMessage(),
        res
      );
    });
  };
}

export const route = new Routes().router;

class HttpCode {
  ok = (): number => 200;
  notFound = (): number => 404;
  error = (): number => 400;
  created = (): number => 201;
  conflict = (): number => 409;
  server = (): number => 500;
}

export default HttpCode;

import HttpCode from "./code";
import HttpMessage from "./message";

const httpCode = new HttpCode();
const httpMessage = new HttpMessage();

const Notification = {
  httpCode,
  httpMessage,
};

export default Notification;

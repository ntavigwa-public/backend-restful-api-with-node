class HttpMessage {
  ok = (): string => "ok";
  notFound = (): string => "not found";
  error = (): string => "error";
  created = (): string => "created";
  conflict = (): string => "exists";
  server = (): string => "server error";
  welcomHomePageMessage = (): string => "Welcome to Home Page";
  PageNotFoundMessage = (): string => "Page not Found";
}

export default HttpMessage;

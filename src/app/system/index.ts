import Notification from "./Notifications";
import Res from "./responses";

const Responses = new Res();

const Sys = {
  Notification,
  Responses,
};

export default Sys;

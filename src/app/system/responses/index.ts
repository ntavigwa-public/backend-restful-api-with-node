import { Response } from "express";

/**
 * Res
 */
class Res {
  /**
   * Ok
   * @author Ntavigwa Bashombe
   * @since 0.001
   *
   * @param {integer} statusCode - status code of the response
   * @param {string} message -  message of the response
   * @param {method} res - response method to pass the response in json
   * @return {object} returns a response
   * @memberof Res
   */
  ok = (statusCode: number, message: string, res: Response): Response => {
    return res.status(statusCode).json({
      status: statusCode,
      message,
    });
  };

  /**
   * Success
   * @author Ntavigwa Bashombe
   * @since 0.001
   *
   * @param {integer} statusCode - status code of the response
   * @param {string} message -  message of the response
   * @param {object} data - data passed to the response
   * @param {method} res - response method to pass the response in json
   * @return {object} returns a response
   * @memberof Res
   */
  success = (
    statusCode: number,
    message: string,
    data: any,
    res: Response
  ): Response => {
    return res.status(statusCode).json({
      status: statusCode,
      message,
      data,
    });
  };

  /**
   * Error
   * @author Ntavigwa Bashombe
   * @since 0.001
   *
   * @param {integer} statusCode - status code of the response
   * @param {string} message -  message of the response
   * @param {method} res - response method to pass the response in json
   * @return {object} returns a response
   * @memberof Res
   */
  error = (statusCode: number, message: string, res: Response): Response => {
    return res.status(statusCode).json({
      status: statusCode,
      message,
    });
  };
}

export default Res;
